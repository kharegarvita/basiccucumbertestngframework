package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import io.github.bonigarcia.wdm.WebDriverManager;

public class driverinitialilze {
	

	private static WebDriver driver ;
	private static String URL = "https://www.google.com";
	
  
	@BeforeTest
	public static WebDriver setup(String browser) 
	 { 
	  if (browser.equalsIgnoreCase("chrome")) 
	  { //setup the browser
	  WebDriverManager.chromedriver().setup(); 
	  driver =new ChromeDriver();
	  driver.manage().deleteAllCookies(); } 
	  else if(browser.equalsIgnoreCase("firefox")) {
	  WebDriverManager.firefoxdriver().setup(); 
	  driver= new FirefoxDriver();
	  driver.manage().deleteAllCookies(); 
	  }
	  return driver;
	  }

//	@AfterClass
	public static void tearDown()
	{
		//driver.close();
		driver.quit();
	}
}