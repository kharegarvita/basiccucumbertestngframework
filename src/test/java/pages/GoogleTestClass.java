package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class GoogleTestClass extends driverinitialilze {

	
	private static By googleserachbox = By.xpath("//input[@name=\"q\"]");
	private static String URL = "https://www.google.com";
	private static String ExpectedTitle= "Google";

	private WebDriver driver =driverinitialilze.setup("chrome");
	// return driver instance in the method of driver creation class and then 
	//call that method and pass browser and store it in a refrence variable of type Webdriber at gloabl level
	
	@Test
	  public void launchUrl() {
	  
	  driver.get(URL); }
	 
	 @Test
	public void  verifyPageTitle()
	{
		String actualScreenTitle = driver.getTitle();
		//Assert.assertEquals(actualScreenTitle, ExpectedTitle);//hard assert in selenium
		SoftAssert sa = new SoftAssert(); //soft assert from testng
		sa.assertEquals(actualScreenTitle, ExpectedTitle);
		sa.assertAll();
	}
	
	 @Test
	public void clickAnytextbox() {
		WebDriverWait wait = new WebDriverWait(driver, 3);
		driver.findElement(googleserachbox).sendKeys("Cucumber Framework");
		
	}
	
	 @Test
	public void closeSession() {
		driverinitialilze.tearDown();
	}
}
