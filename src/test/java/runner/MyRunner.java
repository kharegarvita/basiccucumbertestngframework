package runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
		         features ="src\\test\\resources\\feature\\MyFeature.feature",
		         glue ="stepDef",
		         monochrome = true,
		         plugin = {"html:Reports\\HTMLReports" ,
		        		 "json:Reports\\jsonReports\\Cucumber.json" ,
		        		 "junit:Reports\\JUnitReports\\Cucumber.xml"}
		         
		
		
		)

public class MyRunner extends AbstractTestNGCucumberTests {

}
