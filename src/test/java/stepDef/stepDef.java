package stepDef;

import io.cucumber.java.en.Given;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import pages.GoogleTestClass;
public class stepDef {

	GoogleTestClass obj = new GoogleTestClass();
 
@Given("i want to Launch google site")
public void i_want_to_launch_google_site() {
    // Write code here that turns the phrase above into concrete actions
	System.out.println("Launched google page");
	obj.launchUrl();
	
   
}

@When("i land on homepage")
public void i_land_on_homepage() {
    // Write code here that turns the phrase above into concrete actions
	System.out.println("Landed on google page");
   
}

@Then("I verify the title")
public void i_verify_the_title() {
    // Write code here that turns the phrase above into concrete actions
	System.out.println("Verifying google Title");
	obj.verifyPageTitle();
}

@Then("click on the serach box")
public void click_on_the_serach_box() {
    // Write code here that turns the phrase above into concrete actions
	System.out.println("Click on google search box");
	obj.clickAnytextbox();
	
}


@Then("Close the browser")
public void close_the_browser() {
    // Write code here that turns the phrase above into concrete actions
  obj.closeSession();
  System.out.println("Closed the session");
}

}
